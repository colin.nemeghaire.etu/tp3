import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import { doc } from 'prettier';
import Component from './components/Component.js';
import PizzaForm from './pages/PizzaForm.js';
Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];
Router.menuElement = document.querySelector('.mainMenu');
//Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data; // appel du setter
Router.navigate(document.location.pathname); // affiche la liste des pizzas

/*console.log(document.querySelector('img'));
console.log(document.querySelector('.pizzaFormLink'));
console.log(document.querySelector('.pizzaThumbnail section h4'));
console.log(document.querySelectorAll('nav a'));
console.log(document.querySelectorAll('li'));

console.log(document.querySelectorAll('.pizzaThumbnail h4')[1].textContent);

console.log(document.querySelector('.logo'));*/

document.querySelector('.logo').innerHTML = document.querySelector(
	'.logo'
).innerHTML += "<small>les pizzas c'est la vie</small>";

//console.log(document.querySelectorAll('footer a')[1].origin);

document.querySelectorAll('nav a')[1].classList.add('active');

document.querySelector('.newsContainer').setAttribute('style', 'display:""');

document.querySelector('.closeButton').addEventListener('click', event => {
	document
		.querySelector('.newsContainer')
		.setAttribute('style', 'display:none');
});

window.onpopstate = event => {
	//Router.navigate(document.location.pathname);
	console.log(document.location);
};
