import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form> `;
	}

	mount(element) {
		element.querySelector('button').addEventListener('click', event => {
			event.preventDefault();
			this.submit(event);
		});
		super.mount(element);
	}

	submit(event) {
		const inputValue =
			event.target.parentNode.querySelector('[name="name"]').value;
		if (inputValue === '') {
			alert('Le champ est vide ZEBI !!!');
		} else {
			alert(`Pizza ${inputValue} a été ajoutée`);
			event.target.parentNode.querySelector('[name="name"]').value = '';
		}
	}
}
